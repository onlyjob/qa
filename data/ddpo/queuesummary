#!/usr/bin/perl -w

# queuesummary - take dsc/changes files and decode gpg signatures
#
# Copyright (C) 2005 Christoph Berg <myon@debian.org>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA

# usage: queuesummary <cachedir> <filename ...>

use strict;

my $cache = shift;

# a gpg wrapper

sub gpg_open {
    my $file = shift;
    pipe \*READ, \*WRITE;
    pipe \*EREAD, \*EWRITE;
    my $child;
    unless ($child = fork()) {
	close READ;
	close EREAD;
	open STDIN, "$file" or die "open: $!";
	open STDOUT, '>&WRITE' or die "dup: $!";
	open STDERR, '>&EWRITE' or die "dup: $!";
	exec qw(gpg --homedir /dev/null --status-fd 2 --keyring /dev/null --no-default-keyring);
    }
    close WRITE;
    close EWRITE;
    my $ret;
    while(<READ>) {
	$ret .= $_;
    }
    $ret =~ s/\s+$/\n/s if $ret;
    while(<EREAD>) {
	$ret .= "Uploader: $1\n" if /^\[GNUPG:\] (?:GOOD|ERR)SIG ([0-9A-F]+)/;
    }
    close READ;
    close EREAD;
    return $ret;
}

# read queue

foreach my $file (@ARGV) {
    next unless -e $file;
    my $cachefile = $file;
    $cachefile =~ s!.*/!!;
    $cachefile = "$cache/$cachefile";

    if (-e $cachefile) {
	open F, $cachefile or die "$cachefile: $!";
	while (<F>) { print; }
	close F;
	utime undef, undef, $cachefile; # touch cachefile
    } else {
	my $txt = gpg_open($file) || "";
	print $txt;
	open F, "> $cachefile" or die "$cachefile: $!";
	print F $txt;
	close F;
    }
    print "Filename: $file\n"; # do not cache filename (changes in delayed queue)
    print "\n";
}

# clean up cache

foreach my $file (glob("$cache/*")) {
    if (-M $file > 2) {
	unlink $file;
    }
}

