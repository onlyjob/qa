#!/usr/bin/perl

# Copyright (C) 2019 Paul Wise <pabs@debian.org>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

use warnings;
use strict;
use URI;
use CGI qw(:standard);
use CGI::Carp qw(warningsToBrowser fatalsToBrowser);
use DBD::Pg;

use JSON;

$CGI::POST_MAX = 0;
$CGI::DISABLE_UPLOADS = 1;

my $cgi = CGI->new;
my $dbh = DBI->connect("dbi:Pg:service=qa", 'guest', '',
	{ AutoCommit => 1, RaiseError => 1, PrintError => 1});

my $from_date = '';
$from_date = "$1" if ($cgi->param('from_date') and $cgi->param('from_date') =~ /^(\d+-\d+-\d+)$/);
my $to_date = '';
$to_date = "$1" if ($cgi->param('to_date') and $cgi->param('to_date') =~ /^(\d+-\d+-\d+)$/);

my @packages = split(/[ ,]+/, $cgi->param('packages') // '');
unless (@packages) {
	print header (-status => '500 Bad Parameters', -type => 'text/plain', -charset => 'utf-8');
	print "At least one package is required\n";
	exit;
}

if (!$from_date || !$to_date) {
	$from_date = $dbh->selectrow_array('SELECT min(p.day) FROM popcon_package pp JOIN popcon p ON (pp.id = p.package_id) JOIN popcon_day pd ON (pd.day = p.day) WHERE package = ANY(?) AND in_debian AND (p.vote > 0 OR p.old > 0 OR p.recent > 0 or p.no_files > 0)', undef, \@packages) if !$from_date;
	$to_date = $dbh->selectrow_array('SELECT max(p.day) FROM popcon_package pp JOIN popcon p ON (pp.id = p.package_id) JOIN popcon_day pd ON (pd.day = p.day) WHERE package = ANY(?) AND in_debian AND (p.vote > 0 OR p.old > 0 OR p.recent > 0 or p.no_files > 0)', undef, \@packages) if !$to_date;
	my $uri = URI->new("https://qa.debian.org/cgi-bin/popcon-data");
	$uri->query_form( [ packages => "@packages", from_date => $from_date, to_date => $to_date ], ';' );
	print $cgi->redirect($uri);
	exit;
}

my %data = ();

foreach my $package (@packages) {
	my $rows = $dbh->selectall_hashref("SELECT p.day, p.vote, p.old, p.recent, p.no_files FROM popcon_package pp JOIN popcon p ON (pp.id = p.package_id) JOIN popcon_day pd ON (pd.day = p.day) WHERE package = ? AND p.day BETWEEN SYMMETRIC ? AND ? and in_debian order by p.day", 'day', { Slice => {} }, $package, $from_date, $to_date);
	$data{$package} = $rows;
	foreach my $key (keys %$rows) {
		delete(%$rows{$key}->{day});
	}
}

print header (-type => 'application/json', -charset => 'utf-8');
print encode_json(\%data);
